package org.steinko.jpastart.bootsrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.steinko.jpastart.entity.Book;
import org.steinko.jpastart.repository.BookRepository;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DataInitilizerIT {
	
	@Autowired
	DataInitializer dataInitilaizer;
	
	@Autowired
	BookRepository bookRepository;
	
	@Test
	public void shouldExist() {
		assertNotNull(dataInitilaizer);
	}
	
	
	@Test
	public void shouldSaveBook() throws Exception {
		
		dataInitilaizer.run("");
		List<Book> books = bookRepository.findAll();
		Book book = books.get(0);
		assertEquals(book.getTitle(),"Domain Driven Design");
		book = books.get(1);
		assertEquals(book.getTitle(),"Spring In Action");
	}
	

}
