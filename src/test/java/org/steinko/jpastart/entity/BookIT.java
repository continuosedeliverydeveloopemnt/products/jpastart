package org.steinko.jpastart.entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.steinko.jpastart.repository.BookRepository;


@DataJpaTest
class BookIT {
	
	@Autowired
	BookRepository repository;

	@Test
	void shouldFindByTitle() {
		Book book = new Book("MyCool Bok","ISBN-12345","Aschehauge");
		repository.save(book);
		Book foundBook = repository.findByTitle("MyCool Bok");
		assertEquals(foundBook.getTitle(),"MyCool Bok");
				
	}

}
