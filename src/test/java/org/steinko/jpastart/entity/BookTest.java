package org.steinko.jpastart.entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BookTest {

	@Test
	void shouldExist () {
		assertNotNull( new Book("MyCool Bok","ISBN-12345","Aschehauge"));
	}
	
	@Test
	void shouldGetTitle () {
		 Book book = new Book("MyCool Bok","ISBN-12345","Aschehauge");
		assertEquals( book.getTitle(), "MyCool Bok");
	}
	
	@Test
	void shouldGetIsbn () {
		 Book book = new Book("MyCool Bok","ISBN-12345","Aschehauge");
		assertEquals( book.getIsbn(), "ISBN-12345");
	}
	
	@Test
	void shouldGetPublisher () {
		 Book book = new Book("MyCool Bok","ISBN-12345","Aschehauge");
		assertEquals( book.getPublisher(), "Aschehauge");
	}
	
	

}
