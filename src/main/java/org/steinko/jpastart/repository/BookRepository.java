package org.steinko.jpastart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.steinko.jpastart.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long>{
	Book findByTitle(String title);
}
