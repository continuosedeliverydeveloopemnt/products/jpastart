package org.steinko.jpastart.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.Objects;


@Entity
public class Book {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String title;
	private String isbn;
	private String publisher;
	
	public Book() {

    }
	
	public Book(String title,String isbn,String publisher){
		this.title = title;
		this.isbn = isbn;
		this.publisher = publisher;
	}
	
	 public Long getId() {
	        return id;
	    }

	public String getTitle() {
		return this.title;
	}

	public String getIsbn() {
		return this.isbn;
	}

	public String getPublisher() {
		return this.publisher;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return Objects.equals(id, book.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
